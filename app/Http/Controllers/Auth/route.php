
<?php

Route::get('/', "HomeController@index");

Route::get('contacts', "ContactController@contactList");

Route::get('contact/{id}', "ContactController@getContact");

Route::post('contact', "ContactController@newContact");

Route::delete('contact/{id}', "ContactController@delContact");
?>