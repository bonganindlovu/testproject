<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Contacts;
class ContactsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
 $contacts = Contacts::all()->toArray();
        return view('contacts.index', compact('contacts'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
       return View('contacts.create'); 
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    $this->validate($request, [
       'first_name'  => 'required|min:4', 
        'last_name'  => 'required|min:4', 
        'email'  => 'required|email',
        'cell'  => 'required|min:10|numeric'
        
    ]); 
        $contacts = new Contacts([
         'first_name'  => $request->get('first_name'),  
         'last_name'  => $request->get('last_name'),
         'email'  => $request->get('email'),  
         'cell'  => $request->get('cell')
        ]);
       $contacts->save();
    return redirect()->route('contacts.index')->with('success', 'Contact Saved');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
         $contacts = Contacts::find($id);
        return view('contacts.edit', compact('contacts', 'id'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
         $this->validate($request, [
           'first_name'  => 'required|min:4', 
        'last_name'  => 'required|min:4', 
        'email'  => 'required|email',
        'cell'  => 'required|min:10|numeric'
    
        ]);
        $contact = Contacts::find($id);
        $contact->first_name = $request->get('first_name');
        $contact->last_name = $request->get('last_name');
        $contact->save();
        return redirect()->route('contacts.index')->with('success', 'Contact Updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
       $contact = Contacts::find($id);
        $contact->delete();
        return redirect()->route('contacts.index')->with('success', 'Contact Deleted');
    }
}
