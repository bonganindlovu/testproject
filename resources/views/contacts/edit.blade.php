@extends('layouts.master')
@section('content')
<div class="row">
 <div class="col-md-12">
  <br />
  <h3>Edit Contact</h3>

     <p><a  href="{{('../contacts') }}" >back</a> </p>
  @if(count($errors) > 0)

  <div class="alert alert-danger">
         <ul>
         @foreach($errors->all() as $error)
          <li>{{$error}}</li>
         @endforeach
         </ul>
     </div>
  @endif
  <form method="post" action="{{action('ContactsController@update', $id)}}">
   {{csrf_field()}}
   <input type="hidden" name="_method" value="PATCH" />
   <div class="form-group">
    <input type="text" name="first_name" class="form-control" value="{{$contacts->first_name}}" placeholder="First Name" />
   </div>
   <div class="form-group">
    <input type="text" name="last_name" class="form-control" value="{{$contacts->last_name}}" placeholder="Last Name" />
   </div>
      <div class="form-group">
        <input type="email" name="email" class="form-control" value="{{$contacts->email}}" placeholder="Email">
        </div>
        <div class="form-group">
        <input type="tel" name="cell" class="form-control" value="{{$contacts->cell}}" placeholder="Phone number">
        </div>
   <div class="form-group">
    <input type="submit" class="btn btn-primary" value="Edit" />
   </div>
  </form>
 </div>
</div>

@endsection